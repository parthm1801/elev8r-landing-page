const Logo = ({color}: {color: string}) => (
 <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25.412">
    <g>
      <path d="M 0 25.412 L 0 0 L 25 0 L 25 25.412 Z" fill="transparent"></path>
      <path d="M 12.5 2.118 C 13.428 7.527 17.595 11.763 22.917 12.706 C 17.595 13.649 13.428 17.885 12.5 23.294 C 11.572 17.885 7.405 13.649 2.083 12.706 C 7.405 11.763 11.572 7.527 12.5 2.118 Z" fill={color}></path>
    </g>
 </svg>
);

export default Logo;