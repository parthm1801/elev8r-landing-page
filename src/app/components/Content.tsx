import Logo from "@/assets/Logo";
import styles from "../page.module.css";
import { FORM_LINK } from "@/constants";

const Information = ({imageSrc, heading, text}: {imageSrc: string, heading: string, text: string}) => {
    return (
        <div className={styles.infoContainer}>
            <img src={imageSrc}
                alt="placeholder"
                className={styles.infoImage}/>
            <div className={styles.infoHeader}>{heading}</div>
            <div className={styles.infoText}>{text}</div>
        </div>
    )
}

const Content = ({ctaClick}: {ctaClick: React.Dispatch<React.SetStateAction<boolean>>}) => {
    return (
        <div className={styles.contentContainer}>
            <div className={styles.bannerContainer}>
                <img src="https://ik.imagekit.io/mj77lkuwd/header_photo.jpg?updatedAt=1712383743538"
                    alt="placeholder"
                    className={styles.bannerImage}/>
                <div className={styles.bannerText}>
                    <div className={styles.bannerLogo}>
                        <Logo color="#D976C2"/>
                        <span className={styles.bannerLogoText}>elev8r.tech</span>
                    </div>
                    <div className={styles.bannerHero}>
                        Personalized resume feedback from leading professionals and alumni from top colleges.
                    </div>
                    <button 
                        onClick={() => {window.open(FORM_LINK, '_blank')}}
                        className={styles.bannerCta}>
                            Join waitlist
                    </button>
                </div>
            </div>
            <div className={styles.infoCardsContainer}>
                <div className={styles.infoCardsRow}>
                    <Information
                        imageSrc="https://ik.imagekit.io/mj77lkuwd/upload-folder.png?updatedAt=1712383743521"
                        heading="Power up your resume"
                        text="Upload and boost your resume's potential on Elev8r website."
                    />
                    <Information
                        imageSrc="https://ik.imagekit.io/mj77lkuwd/magnify.png?updatedAt=1712383743477"
                        heading="Find your Elev8r expert "
                        text="Explore our lineup of industry-leading professionals to find the ideal reviewer tailored to your profile."
                    />
                </div>
                <div className={styles.infoCardsRow}>
                    <Information
                        imageSrc="https://ik.imagekit.io/mj77lkuwd/assist.png?updatedAt=1712397301507"
                        heading="Get expert feedback"
                        text="Your Elev8r expert provides a guaranteed detailed feedback on your resume with in 24 hours."
                    />
                    <Information
                        imageSrc="https://ik.imagekit.io/mj77lkuwd/relax.png?updatedAt=1712397344808"
                        heading="Land your Dream job"
                        text="Engage in direct conversations with your Elev8r expert to steer your journey toward landing your dream job."
                    />
                </div>
            </div>
        </div>
    );
}

export default Content;
