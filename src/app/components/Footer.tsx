import styles from "../page.module.css";

const Footer = () => {
    return (
        <div className={styles.footerContainer}>
            <span className={styles.footerText}>&copy; elev8r.tech</span>
            <span className={styles.footerText}>Contact us: contact@elev8r.tech</span>
        </div>
    )
}

export default Footer;
